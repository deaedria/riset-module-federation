import React from "react";

const Button = (props) => (
  <button
    style={{
      backgroundColor: "lavenderblush",
      padding: "10px 10px",
      fontSize: "20px",
    }}
  >
    Hello {props.children}!
  </button>
);

export default Button;
