import React from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "@material-ui/core/styles";
import "./App.css";
// import Button from "Components/Button";
import ThemeStyles from "Components/ThemeStyles";
import HomePage from "./HomePage";

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={ThemeStyles}>
        <header className="App-header">
          {/* <Button>Host</Button> */}
          <HomePage />
        </header>
      </ThemeProvider>
    </div>
  );
}

export default App;

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
