import React from "react";
import { useTheme } from "@material-ui/styles";
import "./App.css";
import ReOvalButton from "Components/ReOvalButton";

function HomePage() {
  const theme = useTheme();

  return (
    <ReOvalButton
      onClick={() => {
        alert("ReOvalButton Clicked");
      }}
      label="CLICK"
      withShadow
      style={{
        backgroundColor: theme.palette.error.main,
      }}
    />
  );
}

export default HomePage;
