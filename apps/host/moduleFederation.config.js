const { dependencies } = require("./package.json");

module.exports = {
  name: "host",
  filename: "remoteEntry.js",
  exposes: {},
  remotes: {
    Components: "remote@http://localhost:5000/remoteEntry.js",
  },
  // shared: require("./package.json").dependencies,
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      import: "react",
      shareScope: "default",
      requiredVersion: dependencies.react,
    },
    "react-dom": {
      singleton: true,
      requiredVersion: dependencies["react-dom"],
    },
  },
};
